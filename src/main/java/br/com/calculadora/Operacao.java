package br.com.calculadora;

public enum Operacao {
  SOMAR {
    @Override
    Integer calcular(Integer valorUm, Integer valorDois) {
      return valorUm + valorDois;
    }
  },
  DIVIDIR {
    @Override
    Integer calcular(Integer valorUm, Integer valorDois) {
      return valorUm / valorDois;
    }
  };

  abstract Integer calcular(Integer valorUm, Integer valorDois);
}
