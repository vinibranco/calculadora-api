package br.com.calculadora;

import static br.com.calculadora.Operacao.DIVIDIR;
import static br.com.calculadora.Operacao.SOMAR;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculadoraController {

  @GetMapping("/v1/adicao")
  public Integer somar(final @RequestParam List<Integer> valores) {
    return valores.stream().reduce((valorUm, valorDois) -> SOMAR.calcular(valorUm, valorDois))
        .orElseThrow(() -> new IllegalArgumentException("Erro ao efetuar a operação de soma"));
  }

  @GetMapping("/v1/divisao")
  public Integer dividir(final @RequestParam List<Integer> valores) {
    return valores.stream()
        .reduce((valorUm, valorDois) -> DIVIDIR.calcular(valorUm, valorDois))
        .orElseThrow(() -> new IllegalArgumentException("Erro ao efetuar a operação de divisão"));
  }

}
