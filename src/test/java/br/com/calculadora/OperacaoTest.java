package br.com.calculadora;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class OperacaoTest {

  @Test
  @DisplayName("Quando informo valor 1 e 2 ao somar o resultado será 3")
  void deveSomarDoisValores() {
    final int valorSomado = Operacao.SOMAR.calcular(1, 2);

    assertEquals(3, valorSomado);
  }

  @Test
  @DisplayName("Quando informo valor 100 e 2 ao dividir o resultado será 50")
  void deveSomarDividirValores() {
    final int valorDividido = Operacao.DIVIDIR.calcular(100, 2);

    assertEquals(50, valorDividido);
  }

  @Test
  @DisplayName("Não é possível dividir por ZERO deve acusar erro")
  void erroAoTentarDividirPorZero() {

    assertThrows(ArithmeticException.class, () -> Operacao.DIVIDIR.calcular(100, 0));
  }

}