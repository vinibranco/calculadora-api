package br.com.calculadora;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class CalculadoraControllerIT {

  @Autowired
  private MockMvc mvc;

  @Test
  @DisplayName("Quando informo o valor 20 e 1 ao chamar rota de soma o resultado será 21 e o status code é Ok")
  void rotaAdicaoComSucesso() throws Exception {

    mvc.perform(
        MockMvcRequestBuilders.get("/v1/adicao?valores=1,20"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$").value(21));
  }

  @Test
  @DisplayName("Quando informo o valor A e 1 deve retornar status code de BadRequest")
  void rotaAdicaoParametrosInvalidos() throws Exception {

    mvc.perform(
        MockMvcRequestBuilders.get("/v1/adicao?valores=a,20"))
        .andExpect(MockMvcResultMatchers.status().isBadRequest())
        .andExpect(jsonPath("$").doesNotExist());
  }

}
